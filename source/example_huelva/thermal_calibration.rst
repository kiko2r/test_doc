For calibration of the thermal image we have the following data:

* Thermal image of the study area in Huelva, each pixel of the image represents the temperature value in degrees kelvin. **thld_training\\huelva\\Z1_holmOak\\Temperature\\t_huelvaZ1.tif**
* Set of calibration values ​​stored in a vector file. Each item has the id and the value of collected temperature (in degrees Kelvin). **thld_training\\huelva\\Z1_holmOak\\Temperature\\t_aois\\t_calibration.shp**

We proceed to load both the image and the shape to QGIS interface. To load the image in the main menu (Layer - Add Raster Layer ...) to load the shape of points (Layer - Add Vector Layer ...).
    
.. figure:: example_huelva/images/thermal_img_Huelva.png
   :align: center
    
   Thermal image of Huelva and attribute table containing the field thermal data

We proceed to perform the calibration of the thermal image through Thermolidar tool toolbox **[Processing] Thermal - Thermal Calibration**.

.. figure:: images/thld_toolbox/thermal_calibration.png
   :width: 285px
   :figwidth: 700px
   :align: center
   
   Thermal calibration tool

We introduce the values ​​as shown in the interface:
    
.. figure:: example_huelva/images/iu_thermal_calibration.png
   :align: center

   Interface of the Thermal Calibration module
    
We obtain as output calibration file of the thermal image, as shown in the following figure:

.. figure:: example_huelva/images/thermal_calibration_out.png
   :width: 450px
   :figwidth: 700px
   :align: center

   Thermal calibration output