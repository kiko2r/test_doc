**********
SPDRMNOISE
**********
 spdrmnoise SPDLib 3.1.0, Copyright (C) 2013 Sorted Pulse Library (SPD) This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions; See website (http://www.spdlib.org). Bugs are to be reported on the trac or directly to spdlib-develop@lists.sourceforge.net

USAGE
=====
``spdrmnoise  -o <String> -i <String>  [--grellow <float>] [--grelup  <float>] [--rellow <float>]  [--relup <float>] [--abslow  <float>] [--absup <float>] [-c  <unsigned int>] [-r <unsigned int>]  [--] [--version] [-h]``


Where
-----
-o <String>, --output <String>  (required)  The output SPD file.
-i <String>, --input <String>  (required)  The input SPD file.
--grellow <float>  Global relative (to median) lower threshold for returns which are to  be removed.
--grelup <float>  Global relative (to median) upper threshold for returns which are to  be removed.
--rellow <float>  Relative (to median) lower threshold for returns which are to be  removed.
--relup <float>  Relative (to median) upper threshold for returns which are to be  removed.
--abslow <float>  Absolute lower threshold for returns which are to be removed.
--absup <float>  Absolute upper threshold for returns which are to be removed.
-c <unsigned int>, --blockcols <unsigned int>  Number of columns within a block (Default 0) - Note values greater  than 1 result in a non-sequencial SPD file.
-r <unsigned int>, --blockrows <unsigned int>  Number of rows within a block (Default 100)
--ignore_rest  Ignores the rest of the labeled arguments following this flag.
--version  Displays version information and exits.
-h, --help  Displays usage information and exits.


NAME
====
Remove vertical noise from LiDAR datasets: **spdrmnoise**