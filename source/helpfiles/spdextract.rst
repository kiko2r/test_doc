**********
SPDEXTRACT
**********
 spdextract SPDLib 3.1.0, Copyright (C) 2013 Sorted Pulse Library (SPD) This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions; See website (http://www.spdlib.org). Bugs are to be reported on the trac or directly to spdlib-develop@lists.sourceforge.net

USAGE
=====
``spdextract  -o <String> -i <String>  [--max] [--min] [--return <ALL|FIRST|LAST|NOTFIRST|FIRSTLAST>]  [--class <unsigned int>] [-b  <float>] [-c <unsigned int>] [-r  <unsigned int>] [--] [--version]  [-h]``


Where
-----
-o <String>, --output <String>  (required)  The output SPD file.
-i <String>, --input <String>  (required)  The input SPD file.
--max  Extract only the maximum returns (within the bin and therefore only  available for SPD file, not UPD).
--min  Extract only the minimum returns (within the bin and therefore only  available for SPD file, not UPD).
--return <ALL|FIRST|LAST|NOTFIRST|FIRSTLAST>  The return(s) of interest
--class <unsigned int>  Class of interest (Ground == 3; Not Ground == 104)
-b <float>, --binsize <float>  Bin size for processing and output image (Default 0) - Note 0 will use  the native SPD file bin size.
-c <unsigned int>, --blockcols <unsigned int>  Number of columns within a block (Default 0) - Note values greater  than 1 result in a non-sequencial SPD file.
-r <unsigned int>, --blockrows <unsigned int>  Number of rows within a block (Default 100)
--ignore_rest  Ignores the rest of the labeled arguments following this flag.
--version  Displays version information and exits.
-h, --help  Displays usage information and exits.


NAME
====
Extract returns and pulses which meet a set of criteria: **spdextract**