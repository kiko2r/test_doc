**********
SPDLASTEST
**********
 spdlastest SPDLib 3.1.0, Copyright (C) 2013 Sorted Pulse Library (SPD) This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions; See website (http://www.spdlib.org). Bugs are to be reported on the trac or directly to spdlib-develop@lists.sourceforge.net

USAGE
=====
``spdlastest  {-p|-c|-f} -i <String> [-n  <unsigned int>] [-s <unsigned int>]  [--] [--version] [-h]``


Where
-----
-p, --print  (OR required)  Print a selction of pulses from LAS file.

**-- or --**
-c, --count  (OR required)  Count the number of pulses in LAS file.

**-- or --**
-f, --notfirst  (OR required)  Print the returns which start a pulse with point IDs  greater than 1

-i <String>, --input <String>  (required)  The input SPD file.
-n <unsigned int>, --number <unsigned int>  Number of pulses to be printed out (Default 10)
-s <unsigned int>, --start <unsigned int>  Starting pulse index (Default 0)
--ignore_rest  Ignores the rest of the labeled arguments following this flag.
--version  Displays version information and exits.
-h, --help  Displays usage information and exits.


NAME
====
Print data pulses from a LAS file - for debugging: **spdlastest**