*******
SPDPROJ
*******


USAGE
=====
``spdproj  {--proj4 <string>|--proj4pretty  <string>|--image <string>|--imagepretty <string>|--spd  <string>|--spdpretty <string>|--epsg <int>|--epsgpretty <int>|--shp <string>|--shppretty  <string>} [--] [--version] [-h]``


Where
-----
--proj4 <string>  (OR required)  Enter a proj4 string (to print WKT)

**-- or --**
--proj4pretty <string>  (OR required)  Enter a proj4 string (to print Pretty WKT)

**-- or --**
--image <string>  (OR required)  Print the WKT string associated with the input image.

**-- or --**
--imagepretty <string>  (OR required)  Print the WKT (to print Pretty WKT) string associated  with the input image.

**-- or --**
--spd <string>  (OR required)  Print the WKT string associated with the input spd  file.

**-- or --**
--spdpretty <string>  (OR required)  Print the WKT (to print Pretty WKT) string associated  with the input spd file.

**-- or --**
--epsg <int>  (OR required)  Print the WKT string associated with the EPSG code  provided.

**-- or --**
--epsgpretty <int>  (OR required)  Print the WKT (to print Pretty WKT) string associated  with the EPSG code provided.

**-- or --**
--shp <string>  (OR required)  Print the WKT string associated with the input ESRI  shapefile.

**-- or --**
--shppretty <string>  (OR required)  Print the WKT (to print Pretty WKT) string associated  with the input ESRI shapefile.

--ignore_rest  Ignores the rest of the labeled arguments following this flag.
--version  Displays version information and exits.
-h, --help  Displays usage information and exits.


NAME
====
Print and convert projection strings: **spdproj**