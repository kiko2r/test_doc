************
SPDDEFHEIGHT
************
 spddefheight SPDLib 3.1.0, Copyright (C) 2013 Sorted Pulse Library (SPD) This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions; See website (http://www.spdlib.org). Bugs are to be reported on the trac or directly to spdlib-develop@lists.sourceforge.net

USAGE
=====
``spddefheight  {--interp|--image} -o  <String> [-e <String>] -i <String>  [--idxres <float>] [--thinres  <float>] [--ptsperbin  <uint_fast16_t>] [--thin]  [--tpsnopts <uint_fast16_t>]  [--tpsRadius <float>]  [--stdDevRadius <float>]  [--largeRadius <float>]  [--smallRadius <float>]  [--stddevThreshold <float>] [--in  <TIN_PLANE|NEAREST_NEIGHBOR|NATURAL_NEIGHBOR|STDEV_MULTISCALE|TPS_RAD|TPS_PTNO>] [-b <float>]  [--overlap <uint_fast16_t>] [-c  <unsigned int>] [-r <unsigned int>]  [--] [--version] [-h]``


Where
-----
--interp  (OR required)  Use interpolation of the ground returns to calculate  ground elevation

**-- or --**
--image  (OR required)  Use an image which defines the ground elevation.

-o <String>, --output <String>  (required)  The output file.
-e <String>, --elevation <String>  The input elevation image.
-i <String>, --input <String>  (required)  The input SPD file.
--idxres <float>  Resolution of the grid index used for some interpolates
--thinres <float>  Resolution of the grid used to thin the point cloud
--ptsperbin <uint_fast16_t>  The number of point allowed within a grid cell following thinning
--thin  Thin the point cloud when interpolating
--tpsnopts <uint_fast16_t>  TPS: (TPS_RAD - minimum) Number of points to be used by TPS algorithm
--tpsRadius <float>  TPS: (TPS_PTNO - maximum) Radius used to retrieve data in TPS  algorithm
--stdDevRadius <float>  STDEV_MULTISCALE: Radius used to calculate the standard deviation
--largeRadius <float>  STDEV_MULTISCALE: Large radius to be used when standard deviation is  low
--smallRadius <float>  STDEV_MULTISCALE: Smaller radius to be used when standard deviation is  high
--stddevThreshold <float>  STDEV_MULTISCALE: Standard Deviation threshold
--in <TIN_PLANE|NEAREST_NEIGHBOR|NATURAL_NEIGHBOR|STDEV_MULTISCALE|TPS_RAD|TPS_PTNO>  The interpolator to be used.
-b <float>, --binsize <float>  Bin size for processing and output image (Default 0) - Note 0 will use  the native SPD file bin size.
--overlap <uint_fast16_t>  Size (in bins) of the overlap between processing blocks (Default 10)
-c <unsigned int>, --blockcols <unsigned int>  Number of columns within a block (Default 0) - Note values greater  than 1 result in a non-sequencial SPD file.
-r <unsigned int>, --blockrows <unsigned int>  Number of rows within a block (Default 100)
--ignore_rest  Ignores the rest of the labeled arguments following this flag.
--version  Displays version information and exits.
-h, --help  Displays usage information and exits.


NAME
====
Define the height field within pulses and points: **spddefheight**