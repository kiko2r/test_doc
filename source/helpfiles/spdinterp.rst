*********
SPDINTERP
*********
 spdinterp SPDLib 3.1.0, Copyright (C) 2013 Sorted Pulse Library (SPD) This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions; See website (http://www.spdlib.org). Bugs are to be reported on the trac or directly to spdlib-develop@lists.sourceforge.net

USAGE
=====
``spdinterp  {--dtm|--chm|--dsm|--amp}  {--topo|--height|--other} -o  <String> -i <String> [--rbflayers  <unsigned int>] [--rbfradius  <double>] [--idxres <float>]  [--thinres <float>] [--ptsperbin  <uint_fast16_t>] [--thin]  [--tpsnopts <uint_fast16_t>]  [--tpsRadius <float>]  [--stdDevRadius <float>]  [--largeRadius <float>]  [--smallRadius <float>]  [--stddevThreshold <float>] [--in  <TIN_PLANE|NEAREST_NEIGHBOR|NATURAL_NEIGHBOR|STDEV_MULTISCALE|TPS_RAD|TPS_PTNO|RBF>] [-f  <string>] [-b <float>] [--overlap  <uint_fast16_t>] [-c <unsigned  int>] [-r <unsigned int>] [--]  [--version] [-h]``


Where
-----
--dtm  (OR required)  Interpolate a DTM image

**-- or --**
--chm  (OR required)  Interpolate a CHM image.

**-- or --**
--dsm  (OR required)  Interpolate a DSM image.

**-- or --**
--amp  (OR required)  Interpolate an amplitude image.

--topo  (OR required)  Use topographic elevation

**-- or --**
--height  (OR required)  Use height above ground elevation.

**-- or --**
--other  (OR required)  Interpolator is not using height.

-o <String>, --output <String>  (required)  The output SPD file.
-i <String>, --input <String>  (required)  The input SPD file.
--rbflayers <unsigned int>  The number of layers used within the RBF interpolator
--rbfradius <double>  The radius used within the RBF interpolator
--idxres <float>  Resolution of the grid index used for some interpolaters
--thinres <float>  Resolution of the grid used to thin the point cloud
--ptsperbin <uint_fast16_t>  The number of point allowed within a grid cell following thinning
--thin  Thin the point cloud when interpolating
--tpsnopts <uint_fast16_t>  TPS: (TPS_RAD - minimum) Number of points to be used by TPS algorithm
--tpsRadius <float>  TPS: (TPS_PTNO - maximum) Radius used to retrieve data in TPS  algorithm
--stdDevRadius <float>  STDEV_MULTISCALE: Radius used to calculate the standard deviation
--largeRadius <float>  STDEV_MULTISCALE: Large radius to be used when standard deviation is  low
--smallRadius <float>  STDEV_MULTISCALE: Smaller radius to be used when standard deviation is  high
--stddevThreshold <float>  STDEV_MULTISCALE: Standard Deviation threshold
--in <TIN_PLANE|NEAREST_NEIGHBOR|NATURAL_NEIGHBOR|STDEV_MULTISCALE|TPS_RAD|TPS_PTNO|RBF>  The interpolator to be used.
-f <string>, --format <string>  Image format (GDAL driver string), Default is ENVI.
-b <float>, --binsize <float>  Bin size for processing and output image (Default 0) - Note 0 will use  the native SPD file bin size.
--overlap <uint_fast16_t>  Size (in bins) of the overlap between processing blocks (Default 10)
-c <unsigned int>, --blockcols <unsigned int>  Number of columns within a block (Default 0) - Note values greater  than 1 result in a non-sequencial SPD file.
-r <unsigned int>, --blockrows <unsigned int>  Number of rows within a block (Default 100)
--ignore_rest  Ignores the rest of the labeled arguments following this flag.
--version  Displays version information and exits.
-h, --help  Displays usage information and exits.


NAME
====
Interpolate a raster elevation surface: **spdinterp**