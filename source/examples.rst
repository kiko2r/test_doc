=========
 Examples
=========

Case Study 1 - Huelva (Spain)
=============================

Thermal Processing
------------------

Thermal Calibration
~~~~~~~~~~~~~~~~~~~

.. include:: ./example_huelva/thermal_calibration.rst

Tc - Ta
~~~~~~~

.. include:: ./example_huelva/tc_ta.rst


Data analysis
-------------

Health condition levels
~~~~~~~~~~~~~~~~~~~~~~~
.. include:: ./example_huelva/hcl.rst

Structurally homogenous forest units
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. include:: ./example_huelva/shfu.rst

Forest Health Classification
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. include:: ./example_huelva/fhc.rst


Case Study 2 - Almería (Spain)
=============================


Thermal Processing
------------------

Thermal Calibration
~~~~~~~~~~~~~~~~~~~

.. include:: ./example_almeria/thermal_calibration.rst

Tc - Ta
~~~~~~~

.. include:: ./example_almeria/tc_ta.rst


Data analysis
-------------

Health condition levels
~~~~~~~~~~~~~~~~~~~~~~~
.. include:: ./example_almeria/hcl.rst

Structurally homogenous forest units
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. include:: ./example_almeria/shfu.rst

Forest Health Classification
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. include:: ./example_almeria/fhc.rst