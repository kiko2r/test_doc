We introduce the input parameters in the user interface:

* **Input layer**: Calibrated thermal raster (generated in the previous section)
* **Air temperature**: Constant air temperature measure at flight time. In this case, the air temperature is considered to 293.15 degrees kelvin
	
.. figure:: example_almeria/images/iu_tc_ta.png
	:align: center
	
	Interface of the Tc-Ta module

.. figure:: example_almeria/images/thermal_img_Filabres.png
	:align: center
	
	Output thermal image of Sierra de los Filabres
