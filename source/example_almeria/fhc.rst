Unsupervised pixel-based classification
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Through this tool raster classify the temperature as many classes as you specify. Temperature classification is performed based on homogeneous units, so many output raster have been defined as SHFU be obtained. 

In our case, we obtain three raster classified. Each raster has associated many categories defined temperature.

.. figure:: example_almeria/images/fhc_unsupervised_pixel.png
	:align: center
	
	Interface of the “Unsupervised pixel-based classification” module

We get as output:

.. figure:: example_almeria/images/fhc_unsupervised_pixel_out.png
	:align: center
	
.. figure:: example_almeria/images/fhc_unsupervised_pixel_out2.png
	:align: center
	
.. figure:: example_almeria/images/fhc_unsupervised_pixel_out3.png
	:align: center
	
.. figure:: example_almeria/images/fhc_unsupervised_pixel_out4.png
	:align: center
	
Unsupervised object-based classification
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
In the same way that the pixel-oriented, this tool classification raster classify the temperature as many classes as you specify. Temperature classification is performed based on homogeneous units, so many output raster have been defined as SHFU be obtained. 

In our case, we obtain three raster classified. Each raster has associated many categories defined temperature. 

The difference from the previous tool, is that instead of being classified pixels are classified objects. The classification is made based on the mean value of each of the objects.

.. figure:: example_almeria/images/fhc_unsupervised_object.png
	:align: center
	
	Interface of the “Unsupervised object-based classification” module

We get as output:

.. figure:: example_almeria/images/fhc_unsupervised_object_out.png
	:align: center
	
.. figure:: example_almeria/images/fhc_unsupervised_object_out2.png
	:align: center
	
.. figure:: example_almeria/images/fhc_unsupervised_object_out3.png
	:align: center
	
.. figure:: example_almeria/images/fhc_unsupervised_object_out4.png
	:align: center