spdinfo SPDLib 3.1.0, Copyright (C) 2013 Sorted Pulse Library (SPD)
This program comes with ABSOLUTELY NO WARRANTY. This is free software,
and you are welcome to redistribute it under certain conditions; See
website (http://www.spdlib.org). Bugs are to be reported on the trac
or directly to spdlib-develop@lists.sourceforge.net
File Path: /home/roberto/Documents/Thermolidar/tutorial/QueenElisabeth_example.spd
File Type: Sequencial index
File Format Version: 2.1
Point Version: 2
Pulse Version: 2
Generating Software: SPDLIB
File Signature: LASF
Spatial Reference: PROJCS["OSGB 1936 / British National Grid",GEOGCS["OSGB 1936",DATUM["OSGB_1936",SPHEROID["Airy 1830",6377563.396,299.3249753150316,AUTHORITY["EPSG","7001"]],AUTHORITY["EPSG","6277"]],PRIMEM["Greenwich",0],UNIT["degree",0.0174532925199433],AUTHORITY["EPSG","4277"]],PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",49],PARAMETER["central_meridian",-2],PARAMETER["scale_factor",0.9996012717],PARAMETER["false_easting",400000],PARAMETER["false_northing",-100000],UNIT["metre",1,AUTHORITY["EPSG","9001"]],AUTHORITY["EPSG","27700"]]
Creation Time (YYYY:MM:DD HH:MM:SS): 2013:12:2:16:21:39
Capture Time (YYYY:MM:DD HH:MM:SS): 0:0:0:0:0:0
Index Type: Cartesian
The file contains: 
	 Contains discrete returns
Number of Points = 1768222
Number of Pulses = 1697708
BBOX [xmin, ymin, xmax, ymax]: [255500,703999.999998,255999.99,704499.989998]
BBOX [Azimuth Min, Zenith Min, Azimuth Max, Zenith Max]: [0,0,0,0]
Z : [91.7799987793,195.649993896]
Range: [0,0]
Scanline: [0,0]
Scanline Idx: [0,0]
Gridding [xSize,ySize] Bin Size: [501,501] 1
Wavelengths:
	8.15705902824e-09
Bandwidths:
	8.15705902824e-09
Pulse Repetition Freq: 0
Beam Divergance: 0
Sensor Height: 0
Footprint: 0
Max. Scan Angle: 0
Waveform Bin Resolution: 32
Temporal Waveform Bin Spacing: 0
Sensor Speed: 0
Sensor Scanrate: 0
Pulse Density: 0
Point Density: 0
Pulse cross track spacing: 0
Pulse along track spacing: 0
Pulse angular spacing azimuth: 0
Pulse angular spacing zenith: 0
Sensor Aperture Size: 0
Pulse Energy: 0
Field of view: 0
User Meta Data: 

spdinfo - end
