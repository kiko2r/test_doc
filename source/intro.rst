The software
============

ThermoLiDAR software is designed to provide the required tools to integrate Thermal and LiDAR information for the assessment of forest health and production. The software includes different modules and several settings offering different levels of processing to suit each needs. The main functionalities of the software are:

1. Tools for raw LiDAR and Thermal data processing, data exploring, quality assessment and visualisation.
2. Tools to integrate both data sources using suitable data fusion techniques.
3. Spatial statistical tools to analyse biophysical variables of the vegetation based on the integration of LiDAR and Thermal data integration.
4. Tools for mapping forest health condition and forest production dynamics and producing accuracy assessment.

The plugin
----------

ThermoLiDAR has been developed as a QGIS plugin. It has been tested successfully with QGIS 1.8.0-Lisboa and it is now been updated to QGIS 2.0. In order to use ThermoLiDAR, it is necessary to `download <http://www.qgis.org/es/site/forusers/download.html>`_ and install QGIS 1.8.0-Lisboa or QGIS 2.0.0-Dufour <http://www.qgis.

The plug-in incorporates tools for LiDAR data managment and processing including a graphical user interface for the SPDLib_ tools.

.. _SPDLib: http://www.spdlib.org/doku.php
.. _FUSION: http://forsys.cfr.washington.edu/JFSP06/lidar_&_ifsar_tools.htm