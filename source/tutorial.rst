========
Tutorial
========

Introduction
============

This section summarize the description of the software and the practical application of the tools implemented using thermal and lidar data collected in the framework of THREMOLIDAR project. 

The main structure of the software is summarized in the following figure. Following is a detail of the main processes carried out by the software.

.. figure:: images/tutorial/general_flowchart.png
	:align: center
	
**A. Processing**. This package includes tools for conducting the processing of raw Thermal and LiDAR data in order to obtain the products required to achieve the parametric analysis of forest health assessment.

	**A.1. Lidar processing**. LiDAR data tool set for the generation of DSMs, DTMs, DVMs and vegetation statistical derivatives thereof.

	**A.2. Thermal image processing**. Thermal data tool set for the calibration of RAW airborne thermal imaging. In addition, tools provide the possibility of calculating a derivate indicator using the difference of the air temperature minus the crown temperature.

	**A.3. Ortorectification and Lidar/Thermal integration**. Geo-reference the thermal data obtained through LiDAR pulses.


**B. Data Analysis**. This package includes tools for conducting the simultaneous analysis of thermal and lidar data information linked to field data measurements to evaluate the state and trends of forest health. A detailed description of data inputs and processes applied is included in Fig. 2

	**B.1. Forest Stand Segmentation (FSS)**. The processed image is decomposed into regions or objects. Object based delineation algorithms are applied with this tool to define forest stands unis for further study.

	**B.2. Health condition levels (HCL)**. Different physiological indicators from field data measurements are processed with this tool to define the ground truth condition of forest status. Health condition levels are statistically generated based in clustering and subsequently validated by ANOVA.

	**B.3. Structurally homogeneous forest units (SHFU)**. This option provides the tools required for the classification of forest stands structurally different. The data applied to perform this classification is defined by the user. In this report the main average height of the trees estimated based on Lidar data has been applied as input data. Alternatively, user can provide external forest maps in a .shp format type with an attribute of the number of class.

	**B.3. Forest Health Monitoring (FHM)**. The final result of this toolkit is the classification of forest health condition levels.  This process requires the training sample plots analyzed and classified in step 1, the forest stand classification calculated in step 2 (or externally provided) and the thermal image data.  Then, the user can apply a supervised classification of thermal data using the training sample plots classified in different health condition levels and based on a specific structural stand level. Optionally, users can apply a non-supervised classification in case the absence of field data measurements.
	
Thermal Processing
==================
Thermal processing tools data allows a user to perform thermal imaging calibration using the Emissive Empirical Line Method (EELM) which is a common method for airborne thermal data processing based on the ‘In-scene atmospheric correction methods. These approaches were developed to remove atmospheric effects from hyperspectal imaging data allowing the user to utilize similar conditions to the atmosphere state. The advantage of using this type of methods over model-based methods based in radiative transfer theory is that they capture the true state of the atmosphere at the time of data collection and the relative low computational efforts required to perform the corrections (in comparison with radiative transfer models approaches). The main difficulty for in-scene method is getting correctly the field measurements parameters required for the correction algorithm.

The Emissive Empirical Line Method (EEELM) is the infrared extension of the widely known Empirical Line Method (ELM) atmospheric correction. EELM employs a linear regression for each band to relate at-sensor radiance with the ground leaving radiance (GLR) via target emissivity and temperature by generating atmospheric transmission, upwelling radiance, and downwelling radiance terms. EELM requires at least one bright target, one dark target and it is also recommended to measure any intermediate target.

Use Empirical Line Compute Factors calibration to force spectral data to match selected field reflectance spectra. A linear regression is used for each band to equate DN and reflectance. This is equivalent to removing the solar irradiance and the atmospheric path radiance. The following equation shows how the empirical line gain and offset values are calculated.

This tool will also allow the user to calculate the difference between Crown Temperature minus Air temperature (Tc-Ta). This indicator has been widely demonstrated to be related with different physiological indicators such as stem water potential, stomatal conductance or sap flow rate. User need to select as input the thermal imaging and the air temperature collected in the same time of the airborne imaging acquisitions.
	
LiDAR Processing
================

.. figure:: images/WorkFlow.png
	:align: center
	
	[Bunting2013b]_

Convert File Formats - spdtranslate
-----------------------------------

The **spdtranslate** command is one of the key commands associated with SPDLib as it allows for the conversion between the various supported file formats, while it also supports coordinate system conversion. 
..and the definition of the data origin (mainly for TLS data).

Although, the most common use of this tool is converting data to the SPD format. The simplest command for converting to UPD (SPD with a spatial index) is shown below, where the input and output file formats have been specified alongside the field used to attribute each pulse with a location to be used if the pulses where later index (i.e., into an SPD file).::

	spdtranslate --if LAS --of UPD -x FIRST_RETURN -i QueenElisabeth_example.las -o QueenElisabeth_example.spd

If you wish to explicitly define the projection of the SPD file then use the ``--input_proj`` and ``--output_proj`` switch to specify a text file containing the OGC WKT string representing the projection. The following command provides an example where the coordinate system (UK Ordnance Survey national grid) has been specified when converting data to an SPD file::

	spdtranslate --if LAS --of UPD -x FIRST_RETURN --input_proj ./OSGB1936.wkt -i QueenElisabeth_example.las -o QueenElisabeth_example.spd

While the following command converts from WGS84 to UK Ordnance Survey national grid while reading the file and converting to SPD::

	spdtranslate --if LAS --of SPD --convert_proj --input_proj WGS84.wkt --output_proj OSGB1936.wkt -xFIRST_RETURN -b 1 -i QueenElisabeth_example.las -o QueenElisabeth_example.spd

To convert data to the SPD format, where data is indexed on to a 10 m grid the following command is the simplest form.::

	spdtranslate --if LAS --of SPD -x FIRST_RETURN -b 10 -i QueenElisabeth_example.las -o QueenElisabeth_example.spd

Within SEXTANTE Toolbox a double click on *spdtranslate* opens its interface that looks like this:

.. image:: images/Module_spdtranslate.png
	:scale: 75%

Some options are set by default as the format of the input (LAS) and output (SPD) files, and the location used to index pulses (FIRST_RETURN) and the user has to set the other options manually. The two only required options are the input and output file. If the absolute path of the input file is know it can be written directly, otherwise there exists the possibility of search it by clicking the |dotbutton| button on the right. This will open another interface. 

.. image:: images/Window_OpenFile_LAS.png

The output file path is specify in the same way by clicking the |dotbutton| button on the right of the output file field. In this case, the user search for the path where the output file should to be saved into and writes the name of the file. It is worth to notice that the file extension has to be set and it should agree with the output format.

.. image:: images/Window_SaveFile_SPD.png

Once both files have been set the **spdtranslate** interface should look like this

.. image:: images/Module_spdtranslate_example.png
	:scale: 75%

and it should be ready to be executed by clicking **OK**. To be sure **spdtranslate** has worked properly, the user can visualise the new spd file into QGIS by means of the spdimport tool. 

.. image:: images/Module_spdimport.png
	:scale: 75%

To load the spd file click on the |dotbutton| button and the user will be ask to select a file. 

.. image:: images/Window_OpenFile_SPD.png

.. WARNING:: Be sure to select the spd file, otherwise the **spdimport** module will not work

Once the spd file has been loaded, QGIS will show the result in the canvas:

.. image:: images/QGIS_import.png
	:scale: 75%

For more info look at the spdtranslate_ help file.

Temporary files
~~~~~~~~~~~~~~~

In the examples given above the whole input files are read into memory and sorted into the spatial grid before being written output the file. This requires that you have sufficient memory to store the whole dataset and index data structure in memory. If you do not have sufficient memory to complete this operation the file needs to tiled, into blocks which are small enough to fit into memory. Allowing the SPD file to be built in stages, naturally this is slower but once completed it is very fast to make spatial selections within the file and other processing steps can be applied to the whole file with only a relatively small memory footprint. The option to select tiling the file to disk while building the SPD file is ``--temppath`` which is path and base file name while the tiles will be written. In the QGIS graphical user interface this option is called *'Path were temporary file can be written to'*.

Supported File Formats
~~~~~~~~~~~~~~~~~~~~~~

The file formats supported are the ones which we have so far required for our research therefore the current level of support is not intended to encompass all the available formats:

* SPD
* UPD
* LAS
* ASCII

**SPD**
	The *Sorted Pulse Data* (SPD) file format [Bunting2013b]_ has been designed specifically for the storage of LiDAR waveform and discrete return data acquired by **TLS**, **ALS** and space borne systems, and includes support for multiple wavelengths within a single file. The format uses a pulse-based structure as opposed to a solely point-based structure, where pulses contain all the information associated with a transmitted pulse from the sensor. The SPD format also supports 2D spatial indexing of the pulses, where pulses can be referenced using cartesian, spherical or polar coordinate systems and projections

**UPD**
	The *Unsorted Pulse Data* (UPD) files are SPD files without a spatial index.  

**LAS**
	The LAS reader is via LibLAS_ and therefore only supports the discrete return (LAS 1.2) data. LAS 1.2 files are exported through the libLAS library so as with the importer only discrete return data are supported.


**ASCII**
	The ASCII format requires a schema, written in XML, to be supplied. The parse experts a single return per line and the resulting pulses will only contain a single return. The following are some examples of schema’s for common ASCII formats. 


	A schema for the PTS format:

	.. literalinclude:: files/PTS.xml
		:linenos:
		:language: xml

	A schema for the XYZ format:

	.. literalinclude:: files/XYZI.xml
		:linenos:
		:language: xml

Classify Ground Returns
-----------------------

Progressive Morphology Filter - spdpmfgrd
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The **spdpmfgrd** command is an implementation of the progressive morphological filter algorithm of [Zhang2003]_. The algorithm works by generating an initial minimum return raster surface at the bin resolution of the SPD file. Circulate morphological operators of a range of scales. At each scale a morphological closing (erosion + dilation) operation is performed. The new height value from the morphological operator is kept if it is above the elevation difference threshold where the elevation difference threshold is increased between threshold using a slope value.

.. Circulate morphological operators of a range of scales (starting at ``--initfilter`` size and going up in increments of 1 to ``--maxfilter``). At each scale a morphological closing (erosion + dilation) operation is performed. The new height value from the morphological operator is kept if it is above the elevation difference threshold (initialised to ``--initelev`` with maximum value of ``--maxelev``) where the elevation difference threshold is increased between threshold using the ``--slope`` parameter. Finally, to classify the LiDAR returns a buffer threshold (``--grd``) is used such that all the point within the buffer or below the surface are classified as ground. Before the classification, by default, a median filter is applied to the final raster surface. The size of the median filter can be specified with the ``--medianfilter`` parameter and the use of the median filter can be turned off using the ``--nomedian`` filter parameter

Under most circumstances the default parameters for the algorithm will be fit for purpose, but be careful that the bin size used within SPD is not too large as the processing will be at this resolution::

	spdpmfgrd -i QueenElisabeth_example.spd -o QueenElisabeth_example_pmfgrd.spd

The **spdpmfgrd** QGIS interface has only three options to be set. Apart from the input and output files, you can select the class the filter will be applied to.

.. image:: images/Module_spdpmfgrd_example.png
	:scale: 75%

For more info look at the spdpmfgrd_ help file.

Filter points depending on class
++++++++++++++++++++++++++++++++
The ``--class`` option allows the filter to be applied to returns of a particular class (i.e., if you have ground returns classified but it needs tidying up etc). It’s useful for TLS as it can take a thick slice with mcc algorithm and then use the PMF algorithm to tidy that result up to get a good overall ground classification.

Multi-Curvature Classifier – spdmccgrd
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The **spdmccgrd** command is an implementation of the multi-scale curvature algorithm [EvansHudak2007]_. This algorithm was created at the US Forest Service and does a good job at classifying ground returns under a forest canopy while retaining the terrain but it does not differentiate the buildings. Under most circumstances the default parameters for the algorithm will be fit for purpose and it is recommend that you try these first with the following command.::

	$ spdmccgrd -i QueenElisabeth_example.spd -o QueenElisabeth_example_mccgrd.spd

The **spdmccgrd** QGIS interface has only three options to be set. Apart from the input and output files, you can select the class the filter will be applied to.

.. image:: images/Module_spdmccgrd_example1.png
	:scale: 75%

For more info look at the spdmccgrd_ help file.

Filter points depending on class
++++++++++++++++++++++++++++++++
As in the **spdpmfgrd** case, the ``--class`` option allows the filter to be applied to returns of a particular class. It’s useful as it can take a thick slice with PMF algorithm and then use the MCC algorithm to tidy that result up to get a good overall ground classification.

Combining filters
~~~~~~~~~~~~~~~~~
Another option which can improve the ground return classification is to combine more than one filtering algorithm to take advantage of their particular strengths and weaknesses. A particularly useful combination is to first run the PMF algo- rithm where a ‘thick’ slice is taken (e.g., 1 or 2 metres above the raster surface) and then the MCC is applied to find the ground returns (using the ``--class`` 3 option)::

	spdmccgrd -i --class 3 QueenElisabeth_example_pmfgrd.spd -o QueenElisabeth_example_mccgrd.spd

.. image:: images/Module_spdmccgrd_example2.png
	:scale: 75%


.. Curvature Parameter
.. +++++++++++++++++++
.. The curvature threshold is the parameter which decides whether a point is classified as a ground or not. If the curvature threshold is higher then more ground returns will be accepted. There are three parameters which can be edited to control the curvature parameter ``--initcurvetol``, ``--mincurvetol`` and ``--stepcurvetol``. The initial survature tolerance (``--initcurvetol``) has a default value of 1, while the minimum curvature has a default of 0.1 and the step between scales is 0.5. To easily change the behaviour of the algorithm is it best to change the initial curvature parameter first, as shown below:

.. 	spdmccgrd --initcurvetol 2 -i liukdr_20090601_1m.spd -o liukdr_20090601_1m_grd.spd


Define Height Field - spddefheight
----------------------------------
The spddefheight command is used to define the height field within both the pulse and point fields of the SPD data file. This can be done in two ways, the simplest is the use of a DTM of the same resolution as the SPD file bin size. The disadvantage of using a DTM is that it is in effect using a series of spot heights and this can introduce artefacts. Therefore, interpolating a value for each point/pulse generates a continuous surface reducing any artefacts. The recommended approach is to use Natural Neighbour interpolation, as demonstrated in the paper of [BaterCoops2009]_.

Without Interpolation, using DTM
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Using the DTM method the only parameters are the input file and output files. The raster DTM needs to the same resolution as the SPD grid and it can be any raster format supported by the GDAL_ library.

.. spddefheight --dtm -i QueenElisabeth_example_mccgrd.spd -e dtm_1m.tif -o QueenElisabeth_example_height.spd

Interpolation Mode
~~~~~~~~~~~~~~~~~~
The interpolators are the same as those defined within the spdinterp command so look to the `Interpolate DTM and DSM`_ section for details on their use. The recommend command using the *natural neighbour* interpolation algorithm, along with the default parameters is::

	spddefheight --interp --in NATURAL_NEIGHBOR -i QueenElisabeth_example_mccgrd.spd -o QueenElisabeth_example_height.spd

.. image:: images/Module_spddefheight_example.png
	:scale: 75%

.. While with the wider range of options to control the block size used for processing (and therefore memory usage) and the thinning process is given here

.. 	spddefheight --interp --overlap 20 --block 200 --thin --thinres 0.5 --ptsperbin 1 --in NATURAL_NEIGHBOR -i liukdr_40205250_20090601_1m_grd.spd -o liukdr_40205250_20090601_1m_grd_nnht.spd


Interpolate DTM and DSM
-----------------------
The most common product to be created from a LiDAR SPD dataset are Digital Terrain Model (DTM), Digital Surface Model (DSM) and Canopy Height Models (CHM). To produce those products you need to interpolate a raster surface from the classified ground returns and top surface points. A key parameter is the resolution of the raster which is generated, within SPDLib the resolution of the raster needs to be a whole number multiple of the SPD index, for example, if the SPD file has a bin size of 10 m then the the output raster file resolution can be 1, 2 or 5 m but not 3 m.

The same module **spdinterp** will permit to generate DTMs, DSMs and CHMs by choosing the output model option and the type of point elevation. To interpolate topographic point elevations generates a DTM in the next way::

	$ spdinterp --dsm --topo --in NATURAL_NEIGHBOR -f GTiff -b 1 -i QueenElisabeth_example.spd -o DSM.tif

.. image:: images/Module_spdinterp_DTM.png
	:scale: 75%

Moreover, importing the spd files generated so far would not show any different result from that imported at the beginning of this tutorial::

	$ spdinterp --dtm --topo --in NATURAL_NEIGHBOR -f GTiff -b 1 -i QueenElisabeth_example_mccgrd.spd -o DTM.tif

	$ spdinterp --dsm --height --in NATURAL_NEIGHBOR -f GTiff -b 1 -i QueenElisabeth_example_mccgrd.spd -o CHM.tif


.. Hillshade models of the DTM and DSM’s are then created for visualisation.::
.. gdaldem hillshade -of KEA LiDAR_GrdClassed_1m_dtm.kea \ LiDAR_GrdClassed_1m_dtm_hillshade.kea
.. gdaldem hillshade -of KEA LiDAR_GrdClassed_1m_dsm.kea \ LiDAR_GrdClassed_1m_dsm_hillshade.kea

.. image:: images/QGIS_dtm.png
	:scale: 75%

.. image:: images/Module_spdinterp_DSM.png
	:scale: 75%
.. image:: images/QGIS_dsm.png
	:scale: 75%

.. image:: images/Module_spdinterp_CHM.png
	:scale: 75%
.. image:: images/QGIS_chm.png
	:scale: 75%

Generate metrics
----------------

spdmetrics calculates metrics, which can be simple statistical moments, percentiles of the point height or return amplitude, or even count ratios. Mathematical operators can be applied to either other operators or metric primitives to allow a range of LiDAR metrics to be derived. 

.. image:: images/Module_spdmetrics.png

Multiple metrics can be calculated at the same time if listed within an XML. This XML file has to be defined *a priori* with a hierarchical list of metrics and operators. Within the **metrics** tags a list of metrics can be provided by the **metric** tag. Within each metric the **{field** attribute is used to name the raster band or vector attribute. Here is an example of an SPD metrics XML file template containing the maximum, the average, the median, the number of pulses, the canopy cover and the percentile 95th.

.. image:: images/Module_spdmetrics_example.png


.. literalinclude:: files/metrics.xml
		:linenos:
		:language: xml

.. [Bunting2013b] Bunting, P., Armston, J., Clewley, D., Lucas, R. M., 2013. Sorted pulse data (SPD) library. Part II: A processing framework for LiDAR data from pulsed laser systems in terrestrial environments. Computers and Geosciences 56, 207 -- 215.

.. [BaterCoops2009] Bater, C. W., Coops, N. C., 2009. Evaluating error associated with lidar-derived DEM interpolation. Computers and Geosciences 35 (2), pp. 289–300.

.. [EvansHudak2007] Evans, J. S., Hudak, A. T., 2007. A multiscale curvature algorithm for classifying discrete return lidar in forested environments. IEEE Transactions on Geoscience and Remote Sensing 45 (4), pp. 1029 -- 1038.

.. [Zhang2003] Zhang, K., Chen, S., Whitman, D., Shyu, M., Yan, J., Zhang, C., 2003. A progressive morphological filter for removing nonground measurements from airborne LIDAR data. IEEE Transactions on Geoscience and Remote Sensing 41 (4), pp. 872 -- 882.



.. _LibLAS: http://www.liblas.org
.. _GDAL: http://www.gdal.org

.. _spdtranslate: /media/DATA/Thermolidar/tutorial/_build/html/helpfiles/spdtranslate.html
.. _spdpmfgrd: /media/DATA/Thermolidar/tutorial/_build/html/helpfiles/spdpmfgrd.html
.. _spdmccgrd: /media/DATA/Thermolidar/tutorial/_build/html/helpfiles/spdmccgrd.html

.. |dotbutton| image:: images/Button_dots.png
	:scale: 65%