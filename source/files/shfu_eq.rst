*************
SHFU Equation
*************
	To define an equation that characterizes the structure of objects, we use a model consisting of the combination of variables derived from LiDAR, using the following functions and operators:

Operators
---------
	**, Power operator

	+,	Addition operator

	-,	Minus operator

	*,	Multiplication operator

	/,	Division operator	

	%,	Modulo operator


Representation fuctions
-----------------------
* **fabs(x)** Return the absolute value of x.

* **factorial(x)** Return x factorial. Raises ValueError if x is not integral or is negative.

* **fmod(x, y)** Return fmod(x, y), as defined by the platform C library. Note that the Python expression x % y may not return the same result. The intent of the C standard is that fmod(x, y) be exactly (mathematically; to infinite precision) equal to x - n*y for some integer n such that the result has the same sign as x and magnitude less than abs(y). Python’s x % y returns a result with the sign of y instead, and may not be exactly computable for float arguments. For example, fmod(-1e-100, 1e100) is -1e-100, but the result of Python’s -1e-100 % 1e100 is 1e100-1e-100, which cannot be represented exactly as a float, and rounds to the surprising 1e100. For this reason, function fmod() is generally preferred when working with floats, while Python’s x % y is preferred when working with integers.

Power and logarithmic functions
-------------------------------
* **epx** Return e**x.

* **expm1(x)** Return e**x - 1. For small floats x, the subtraction in exp(x) - 1 can result in a significant loss of precision; the expm1() function provides a way to compute this quantity to full precision

* **log(x[, base])** With one argument, return the natural logarithm of x (to base e). With two arguments, return the logarithm of x to the given base, calculated as log(x)/log(base)

* **log1p(x)** Return the natural logarithm of 1+x (base e). The result is calculated in a way which is accurate for xnear zero.

* **log10(x)** Return the base-10 logarithm of x. This is usually more accurate than log(x, 10).

* **pow(x, y)** Return x raised to the power y. Exceptional cases follow Annex ‘F’ of the C99 standard as far as possible. In particular, pow(1.0, x) and pow(x, 0.0) always return 1.0, even when x is a zero or a NaN. If both x and y are finite, x is negative, and y is not an integer then pow(x, y) is undefined, and raisesValueError.

* **sqrt(x)** Return the square root of x.

Trigonometric functions
-----------------------
* **atan(x)**  Return the arc tangent of x, in radians.

* **atan2(y, x)** Return atan(y / x), in radians. The result is between -pi and pi. The vector in the plane from the origin to point (x, y) makes this angle with the positive X axis. The point of atan2() is that the signs of both inputs are known to it, so it can compute the correct quadrant for the angle. For example, atan(1) andatan2(1, 1) are both pi/4, but atan2(-1, -1) is -3*pi/4.

* **cos(x)** Return the cosine of x radians.

* **hypot(x, y)** Return the Euclidean norm, sqrt(x*x + y*y). This is the length of the vector from the origin to point (x,y).

* **sin(x)** Return the sine of x radians.

* **tan(x)** Return the tangent of x radians.


Constant
--------
* **pi** The mathematical constant π = 3.141592..., to available precision.

* **e** The mathematical constant e = 2.718281..., to available precision.

Examples
---------
* **E_P95**

* **E_P80*2 + E_CV*2.34 + 5.89**

