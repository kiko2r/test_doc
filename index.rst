.. Deliverable_2.3 documentation master file, created by
   sphinx-quickstart on Mon May 20 14:25:20 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

ThermoLiDAR Plugin
==================

.. sidebar:: Summary

    :Release: |release|
    :Date: |today|
    :Authors: **Roberto Antolin**; **FJ Romero**,  
    :Target: users
    :status: some mature, some in progress

.. toctree::
   :maxdepth: 3
   :numbered:

   source/intro.rst
   source/installation.rst
   source/tutorial.rst
   source/user_manual.rst
   source/examples.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


